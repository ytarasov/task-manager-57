package ru.t1.ytarasov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.ytarasov.tm.api.service.IConnectionService;
import ru.t1.ytarasov.tm.api.service.IPropertyService;
import ru.t1.ytarasov.tm.api.service.model.IProjectService;
import ru.t1.ytarasov.tm.api.service.model.IUserService;
import ru.t1.ytarasov.tm.configuration.ServerConfiguration;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.ytarasov.tm.exception.field.*;
import ru.t1.ytarasov.tm.marker.UnitCategory;
import ru.t1.ytarasov.tm.model.Project;
import ru.t1.ytarasov.tm.model.User;
import ru.t1.ytarasov.tm.service.ConnectionService;
import ru.t1.ytarasov.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.List;

@Category(UnitCategory.class)
public class ProjectServiceTest {

    @NotNull
    private static final String TEST_PROJECT_NAME = "test";

    @NotNull
    private static final String TEST_PROJECT_DESCRIPTION = "test";

    @NotNull
    private static final String TEST_ADD_PROJECT_NAME = "test add";

    @NotNull
    private static final String TEST_ADD_PROJECT_DESCRIPTION = "test add";

    @NotNull
    private static final String TEST_REMOVE_PROJECT_NAME = "test remove";

    @NotNull
    private static final String TEST_REMOVE_PROJECT_DESCRIPTION = "test remove";

    @NotNull
    private static final String TEST_REMOVE_BY_ID_PROJECT_NAME = "test remove by id";

    @NotNull
    private static final String TEST_REMOVE_BY_ID_PROJECT_DESCRIPTION = "test remove by id";

    @NotNull
    private static final String TEST_CHANGE_STATUS_PROJECT_NAME = "test change status";

    @NotNull
    private static final String TEST_CHANGE_STATUS_DESCRIPTION = "test change status";

    @NotNull
    @Autowired
    private static IPropertyService propertyService;

    @NotNull
    private static IProjectService projectService;

    @NotNull
    private static IUserService userService;

    private static User user;

    @Nullable
    private final Project nullProject = null;

    @Nullable
    private final List<Project> nullList = null;

    @NotNull
    private final List<Project> emptyList = new ArrayList<>();

    @Nullable
    private final String nullId = null;

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        userService = context.getBean(IUserService.class);
        projectService = context.getBean(IProjectService.class);
        user = userService.create("test_project", "test_project", "test_project@tst.ru", Role.USUAL);
        projectService.create(user.getId(), TEST_PROJECT_NAME, TEST_PROJECT_DESCRIPTION);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        projectService.clear();
        userService.remove(user);
    }

    @Test
    public void getSize() throws Exception {
        Assert.assertEquals(projectService.findAll().size(), projectService.getSize().intValue());
    }

    @Test
    public void findAll() throws Exception {
        Assert.assertEquals(projectService.getSize().intValue(), projectService.findAll().size());
    }

    @Test
    public void add() throws Exception {
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.add(nullProject));
        final int expectedSize = projectService.getSize().intValue() + 1;
        @NotNull final Project project = new Project();
        project.setName(TEST_ADD_PROJECT_NAME);
        project.setDescription(TEST_ADD_PROJECT_DESCRIPTION);
        project.setStatus(Status.NOT_STARTED);
        project.setUser(user);
        projectService.add(project);
        final int foundSize = projectService.getSize().intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void remove() throws Exception {
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.remove(nullProject));
        @Nullable final Project project = projectService.create(user.getId(), TEST_REMOVE_PROJECT_NAME, TEST_REMOVE_PROJECT_DESCRIPTION);
        Assert.assertNotNull(project);
        final int expectedSize = projectService.getSize().intValue() - 1;
        projectService.remove(project);
        final int foundSize = projectService.getSize().intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void removeById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> projectService.removeById(null));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.removeById(""));
        @Nullable final Project project = projectService.create(user.getId(), TEST_REMOVE_BY_ID_PROJECT_NAME, TEST_REMOVE_BY_ID_PROJECT_DESCRIPTION);
        Assert.assertNotNull(project);
        @NotNull final String id = project.getId();
        final int expectedSize = projectService.getSize().intValue() - 1;
        projectService.removeById(id);
        final int foundSize = projectService.getSize().intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void changeStatus() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> projectService.changeStatusById(user.getId(), null, Status.COMPLETED));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.changeStatusById(user.getId(), "", Status.COMPLETED));
        @Nullable final Project project = projectService.create(user.getId(), TEST_CHANGE_STATUS_PROJECT_NAME, TEST_CHANGE_STATUS_DESCRIPTION);
        Assert.assertNotNull(project);
        @NotNull final String id = project.getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeStatusById(null, id, Status.COMPLETED));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeStatusById("", id, Status.COMPLETED));
        Assert.assertThrows(StatusEmptyException.class, () -> projectService.changeStatusById(user.getId(), id, null));
        projectService.changeStatusById(user.getId(), id, Status.COMPLETED);
        @Nullable final Project changedProject = projectService.findOneById(user.getId(), id);
        Assert.assertNotNull(changedProject);
        Assert.assertEquals(TEST_CHANGE_STATUS_PROJECT_NAME, changedProject.getName());
        Assert.assertEquals(Status.COMPLETED.getDisplayName(), changedProject.getStatus().getDisplayName());
    }

}
