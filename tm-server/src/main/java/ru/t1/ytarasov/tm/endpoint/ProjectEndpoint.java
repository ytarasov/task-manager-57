package ru.t1.ytarasov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.ytarasov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.ytarasov.tm.api.service.dto.IProjectServiceDTO;
import ru.t1.ytarasov.tm.api.service.dto.IProjectTaskServiceDTO;
import ru.t1.ytarasov.tm.api.service.IServiceLocator;
import ru.t1.ytarasov.tm.dto.request.project.*;
import ru.t1.ytarasov.tm.dto.response.project.*;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.dto.model.ProjectDTO;
import ru.t1.ytarasov.tm.dto.model.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Controller
@WebService(endpointInterface = "ru.t1.ytarasov.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    @Autowired
    private IProjectServiceDTO projectService;

    @NotNull
    @Autowired
    private IProjectTaskServiceDTO projectTaskService;

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectCreateResponse createProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCreateRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final ProjectDTO project = projectService.create(userId, name, description);
        return new ProjectCreateResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectListResponse listProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectListRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Sort sort = request.getSort();
        @Nullable final List<ProjectDTO> projects = projectService.findAll(userId, sort);
        return new ProjectListResponse(projects);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectShowByIdResponse showProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectShowByIdRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final ProjectDTO project = projectService.findOneById(userId, projectId);
        return new ProjectShowByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectStartByIdResponse startProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectStartByIdRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getToken();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final ProjectDTO project = projectService.changeProjectStatusById(userId, projectId, Status.IN_PROGRESS);
        return new ProjectStartByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectCompleteByIdResponse completeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCompleteByIdRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final ProjectDTO project = projectService.changeProjectStatusById(userId, projectId, Status.COMPLETED);
        return new ProjectCompleteByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectChangeStatusByIdResponse changeProjectStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectChangeStatusByIdRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final Status status = request.getStatus();
        @Nullable final ProjectDTO project = projectService.changeProjectStatusById(userId, projectId, status);
        return new ProjectChangeStatusByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectUpdateByIdResponse updateProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectUpdateByIdRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final ProjectDTO project = projectService.updateById(userId, projectId, name, description);
        return new ProjectUpdateByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectRemoveByIdResponse removeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectRemoveByIdRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final ProjectDTO project = projectTaskService.removeProjectById(userId, projectId);
        return new ProjectRemoveByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ProjectClearResponse clearProjects(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectClearRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        projectTaskService.clearAllProjects(userId);
        return new ProjectClearResponse();
    }

}
