package ru.t1.ytarasov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @Nullable
    List<Task> findAll(@Nullable final Comparator comparator) throws Exception;

    @Nullable
    List<Task> findAll(@Nullable final Sort sort) throws Exception;

    @Nullable
    List<Task> findAll(@Nullable final String userId, @Nullable final Comparator comparator) throws Exception;

    @Nullable
    List<Task> findAll(@Nullable final String userId, @Nullable final Sort sort) throws Exception;

    @Nullable
    List<Task> findByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception;

    @NotNull
    Task create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) throws Exception;

    @NotNull
    Task updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) throws Exception;

    @NotNull
    Task changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) throws Exception;

}
