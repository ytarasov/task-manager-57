package ru.t1.ytarasov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    EntityManager getEntityManager();

    void add(@NotNull final M model);

    void update(@NotNull final M model);

    void remove(@NotNull final M model);

    void clear();

    @Nullable
    List<M> findAll();

    @Nullable
    M findOneById(@NotNull final String id);

    Long getSize();

    Boolean existsById(@NotNull final String id);

}
