package ru.t1.ytarasov.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.model.AbstractUserOwnedModelDTO;

import java.util.List;

public interface IUserOwnedServiceDTO<M extends AbstractUserOwnedModelDTO> extends IServiceDTO<M> {

    void clear(@Nullable String userId) throws Exception;

    Boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    List<M> findAll(@Nullable String userId) throws Exception;

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id) throws Exception;

    Long getSize(@Nullable String userId) throws Exception;

    @Nullable
    M removeById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    M add(@Nullable String userId, @Nullable M model) throws Exception;

    @Nullable
    M remove(@Nullable String userId, @Nullable M model) throws Exception;

}
