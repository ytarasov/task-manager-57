package ru.t1.ytarasov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.ytarasov.tm.api.EntityConstantDTO;
import ru.t1.ytarasov.tm.api.repository.dto.ITaskRepositoryDTO;
import ru.t1.ytarasov.tm.dto.model.ProjectDTO;
import ru.t1.ytarasov.tm.dto.model.TaskDTO;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
public final class TaskRepositoryDTO
        extends AbstractUserOwnedRepositoryDTO<TaskDTO> implements ITaskRepositoryDTO {

    @Override
    public Long getSize() {
        return entityManager
                .createQuery("SELECT COUNT(t) FROM TaskDTO t", Long.class)
                .getSingleResult();
    }

    @Override
    public Boolean existsById(@NotNull final String id) {
        return entityManager
                .createQuery("SELECT COUNT(1) = 1 FROM TaskDTO t WHERE t.id = :id", Boolean.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public Long getSize(@NotNull final String userId) {
        return entityManager
                .createQuery("SELECT COUNT(t) FROM TaskDTO t WHERE t.userId = :userId", Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public Boolean existsById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = String.format("SELECT COUNT(1) = 1 FROM %s m WHERE m.%s = :id AND m.%s = :userId",
                getTableName(), EntityConstantDTO.COLUMN_ID, EntityConstantDTO.COLUMN_USER_ID);
        return entityManager
                .createQuery("SELECT COUNT(1) = 1 FROM TaskDTO t WHERE t.%s = :id AND t.userId = :userId", Boolean.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll() {
        return entityManager
                .createQuery("FROM TaskDTO", TaskDTO.class)
                .getResultList();
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@NotNull String id) {
        return entityManager
                .createQuery("SELECT p FROM TaskDTO p WHERE p.id = :id", TaskDTO.class)
                .setParameter("id", id)
                .setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @Override
    public @Nullable List<TaskDTO> findAll(@NotNull Comparator comparator) {
        return entityManager
                .createQuery("SELECT t FROM TaskDTO t ORDER BY :sort", TaskDTO.class)
                .setParameter("sort", getSortedColumn(comparator))
                .getResultList();
    }

    @Override
    public @Nullable List<TaskDTO> findAll(@NotNull String userId, @NotNull Comparator comparator) {
        return entityManager
                .createQuery("SELECT t FROM TaskDTO t WHERE t.userId = :userId ORDER BY :sort", TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("sort", getSortedColumn(comparator))
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllTasksByProjectId(@NotNull String userId, @NotNull String projectId) {
        return entityManager
                .createQuery("SELECT t FROM TaskDTO t WHERE t.userId = :userId AND t.projectId = :projectId", TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public @Nullable List<TaskDTO> findAll(@NotNull String userId) {
        return entityManager
                .createQuery("SELECT t FROM TaskDTO t WHERE t.userId = :userId", TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @Nullable TaskDTO findOneById(@NotNull String userId, @NotNull String id) {
        @NotNull final String jpql = String.format("SELECT p FROM %s p WHERE p.%s = :id AND p.%s = :userId",
                getTableName(), EntityConstantDTO.COLUMN_ID, EntityConstantDTO.COLUMN_USER_ID);
        return entityManager
                .createQuery("SELECT t FROM TaskDTO t WHERE t.id = :id AND t.userId = :userId", TaskDTO.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @Override
    protected @NotNull String getTableName() {
        return EntityConstantDTO.TABLE_TASK;
    }

}
