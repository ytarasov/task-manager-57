package ru.t1.ytarasov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ytarasov.tm.api.service.dto.IProjectServiceDTO;
import ru.t1.ytarasov.tm.api.service.dto.IProjectTaskServiceDTO;
import ru.t1.ytarasov.tm.api.service.dto.ITaskServiceDTO;
import ru.t1.ytarasov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.ytarasov.tm.exception.entity.TaskNotFoundException;
import ru.t1.ytarasov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.ytarasov.tm.exception.field.TaskIdEmptyException;
import ru.t1.ytarasov.tm.exception.field.UserIdEmptyException;
import ru.t1.ytarasov.tm.dto.model.ProjectDTO;
import ru.t1.ytarasov.tm.dto.model.TaskDTO;

import java.util.List;

@Service
public final class ProjectTaskServiceDTO implements IProjectTaskServiceDTO {

    @NotNull
    @Autowired
    private IProjectServiceDTO projectService;

    @NotNull
    @Autowired
    private ITaskServiceDTO taskService;

    @Override
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String taskId,
            @Nullable final String projectId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @Nullable final TaskDTO task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        taskService.update(task);
    }

    @NotNull
    @Override
    public ProjectDTO removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @Nullable final ProjectDTO project = projectService.findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        @Nullable List<TaskDTO> tasksToRemove = taskService.findAllTasksByProjectId(userId, project.getId());
        if (tasksToRemove == null) throw new TaskNotFoundException();
        for (@NotNull final TaskDTO task : tasksToRemove) taskService.removeById(task.getId());
        projectService.removeById(projectId);
        return project;
    }

    @Override
    public void clearAllProjects(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        final List<ProjectDTO> projects = projectService.findAll(userId);
        if (projects == null) return;
        @Nullable final List<TaskDTO> tasks = taskService.findAll(userId);
        if (tasks == null) {
            projectService.clear(userId);
            return;
        }
        for (final ProjectDTO project : projects) {
            for (final TaskDTO task : tasks) {
                if (project.getId().equals(task.getProjectId())) taskService.removeById(userId, task.getId());
            }
        }
        projectService.clear(userId);
    }

    @Override
    public void unbindTaskFromProject(
            @Nullable String userId,
            @Nullable final String taskId,
            @Nullable final String projectId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @Nullable final TaskDTO task = taskService.findOneById(userId, taskId);
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        taskService.update(task);
    }

}
