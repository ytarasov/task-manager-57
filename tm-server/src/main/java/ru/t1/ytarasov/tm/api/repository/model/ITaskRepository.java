package ru.t1.ytarasov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @Nullable
    List<Task> findAll(@NotNull final Comparator comparator);

    @Nullable
    List<Task> findAll(@NotNull final String userId, @NotNull final Comparator comparator);

    @Nullable
    List<Task> findByProjectId(@NotNull final String userId, @NotNull final String projectId);

}
