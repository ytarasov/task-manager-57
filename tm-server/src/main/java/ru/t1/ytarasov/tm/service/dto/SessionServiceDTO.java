package ru.t1.ytarasov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.ytarasov.tm.api.repository.dto.ISessionRepositoryDTO;
import ru.t1.ytarasov.tm.api.service.IConnectionService;
import ru.t1.ytarasov.tm.api.service.dto.ISessionServiceDTO;
import ru.t1.ytarasov.tm.exception.entity.SessionNotFoundException;
import ru.t1.ytarasov.tm.exception.field.IdEmptyException;
import ru.t1.ytarasov.tm.dto.model.SessionDTO;
import ru.t1.ytarasov.tm.exception.field.UserIdEmptyException;
import ru.t1.ytarasov.tm.repository.dto.SessionRepositoryDTO;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

@Service
public class SessionServiceDTO
        extends AbstractUserOwnedServiceDTO<SessionDTO, SessionRepositoryDTO> implements ISessionServiceDTO {

    @NotNull
    private ISessionRepositoryDTO getRepository() {
        return context.getBean(ISessionRepositoryDTO.class);
    }

    @Override
    public @Nullable List<SessionDTO> findAll() throws Exception {
        @Nullable final List<SessionDTO> sessions;
        @NotNull final ISessionRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public Long getSize() throws Exception {
        @NotNull final ISessionRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.getSize();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public SessionDTO add(@Nullable SessionDTO session) throws Exception {
        if (session == null) throw new SessionNotFoundException();
        @NotNull final ISessionRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(session);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return session;
    }

    @NotNull
    @Override
    public Collection<SessionDTO> add(@NotNull Collection<SessionDTO> models) throws Exception {
        if (models.isEmpty()) throw new SessionNotFoundException();
        for (@NotNull final SessionDTO model : models) add(model);
        return models;
    }

    @Override
    public @Nullable SessionDTO findOneById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final ISessionRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findOneById(id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear() throws Exception {
        @NotNull final ISessionRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public Boolean existsById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final ISessionRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.existsById(id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @NotNull SessionDTO remove(@Nullable SessionDTO session) throws Exception {
        if (session == null) throw new SessionNotFoundException();
        @NotNull final ISessionRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.remove(session);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return session;
    }

    @Override
    public @Nullable SessionDTO removeById(String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final SessionDTO session = findOneById(id);
        if (session == null) throw new SessionNotFoundException();
        return remove(session);
    }

    @Override
    public void clear(@Nullable String userId) throws Exception {
        @NotNull final ISessionRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public Boolean existsById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final ISessionRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.existsById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<SessionDTO> findAll(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<SessionDTO> sessions;
        @NotNull final ISessionRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll(userId);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public SessionDTO findOneById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final ISessionRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findOneById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public Long getSize(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ISessionRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.getSize(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @Nullable SessionDTO removeById(@Nullable String userId, @Nullable String id) throws Exception {
        @Nullable final SessionDTO session = findOneById(userId, id);
        if (session == null) throw new SessionNotFoundException();
        return remove(session);
    }

    @Override
    public @Nullable SessionDTO add(@Nullable String userId, @Nullable SessionDTO model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public @Nullable SessionDTO remove(@Nullable String userId, @Nullable SessionDTO model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final SessionDTO session = findOneById(userId, model.getId());
        if (session == null) throw new SessionNotFoundException();
        return remove(session);
    }

}
