package ru.t1.ytarasov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ytarasov.tm.dto.request.user.UserLoginRequest;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.event.ConsoleEvent;
import ru.t1.ytarasov.tm.util.TerminalUtil;

@Component
public final class UserLoginListener extends AbstractUserListener {

    @NotNull
    public static final String NAME = "login";

    @NotNull
    public static final String DESCRIPTION = "UserDTO login";

    @Override
    @EventListener(condition = "@userLoginListener.getName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) throws Exception {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin(login);
        request.setPassword(password);
        @Nullable final String token = getAuthEndpoint().login(request).getToken();
        setToken(token);
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
