package ru.t1.ytarasov.tm.dto.response.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.response.AbstractResponse;
import ru.t1.ytarasov.tm.dto.model.UserDTO;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class UserRegistryResponse extends AbstractResponse {

    @Nullable
    private UserDTO user;

}
